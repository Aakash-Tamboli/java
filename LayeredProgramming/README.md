#	DOCUMENTATION

## Overview
here you get to know about `Layered Programming` of My Java mini-projects.

## Why The Layered Programming in Java ?
So, In It industry mostly projects built on Layered Programming concept
because,
It is easy to manage and distribute work according domain specific, programmer.
Easy to migrate our application to Desktop to web, web to mobile and so on. we just need to change presentation layer.


## What I learned during layered programing.
1) DTO and DAO:
	DTO stands for Data Transfer Object, it is used to send data to one layer to another layer wrapping into the object.
	DAO stands for Data Access Object, it is used to perform CRUD operation on data layer side, In some organization used DL named instead of DAO.
2) Data Base:
	Using Data base like sql or any RDBMS application we can store data into file without writing File handling code.



### Reason to learn Layered Programing
Because In real-life or In Industry mostly work done on Layered Programing Concept

### Credit
Thanks to God and founder of thinkingmachine.in that is Mr.Prafull Kelker who give the industry grade knowledge.
