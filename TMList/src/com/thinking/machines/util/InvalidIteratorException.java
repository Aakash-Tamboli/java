package com.thinking.machines.util;
public class InvalidIteratorException extends RuntimeException // because we that our exception class should be type of unchecked exception
{
public InvalidIteratorException(String message)
{
super(message);
}
}