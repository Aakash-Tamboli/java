package com.thinking.machines.util;
public class TMLinkedList<T> implements TMList<T>
{
class TMLinkedListNode<T>
{
public T data;
public TMLinkedListNode next;
public TMLinkedListNode()
{
this.data=null;
this.next=null;
}
}
private TMLinkedListNode start;
private TMLinkedListNode end;
private int size;
public TMLinkedList()
{
this.start=null;
this.end=null;
this.size=0;
}
public void add(T data)
{
TMLinkedListNode<T> node;
node=new TMLinkedListNode<T>();
node.data=data;
if(this.start==null)
{
this.start=node;
this.end=node;
}
else
{
this.end.next=node;
this.end=node;
}
this.size++;
}
public void add(int index,T data)
{
this.insert(index,data);
}
public void insert(int index,T data)
{
if(index<0) throw new ArrayIndexOutOfBoundsException("Invalid index: "+index);
if(index>=this.size)
{
this.add(data);
return;
}
TMLinkedListNode<T> node=new TMLinkedListNode<T>();
node.data=data;
if(index==0)
{
node.next=this.start;
this.start=node;
}
else
{
int i;
TMLinkedListNode<T> t,j;
t=this.start;
j=t;
i=0;
while(i<index)
{
j=t;
t=t.next;
i++;
}
j.next=node;
node.next=t;
}
this.size++;
}
public void removeAll()
{
this.clear();
}
public T removeAt(int index)
{
if(index<0 || index>=this.size) throw new ArrayIndexOutOfBoundsException("Invalid Index: "+index);
TMLinkedListNode<T> dataNode;
T data;
if(this.start==this.end) // only one node
{
dataNode=this.start;
data=dataNode.data;
this.start=null;
this.end=null;
this.size=0;
return data;
}
if(index==0) // many but remove 1st
{
dataNode=this.start;
data=dataNode.data;
this.start=this.start.next;
this.size--;
return data;
}
int i;
TMLinkedListNode<T> t,j;
t=this.start;
j=t;
i=0;
while(i<index)
{
j=t;
t=t.next;
i++;
}
data=t.data;
j.next=t.next;
if(t==this.end) this.end=j;
this.size--;
return data;
}
public void clear()
{
// In java We don't need to worry about garbage collection jvm will tacke that thing.
this.size=0;
this.start=null;
this.end=null;
}
public int size()
{
return this.size;
}
public T get(int index)
{
if(index<0 || index>=this.size) throw new ArrayIndexOutOfBoundsException("Invalid index: "+index);
int i=0;
T data;
TMLinkedListNode<T> t;
t=this.start;
while(i<index)
{
t=t.next;
i++;
}
data=t.data;
return data;
}
public void update(int index,T data)
{
if(index<0 || index>=this.size) throw new ArrayIndexOutOfBoundsException("Invalid Index: "+index);
int i;
TMLinkedListNode<T> t;
t=this.start;
i=0;
while(i<index)
{
t=t.next;
i++;
}
t.data=data;
}
public void copyTo(TMList<T> other)
{
other.clear();
for(int i=0;i<this.size();i++) other.add(this.get(i));
}
public void copyFrom(TMList<T> other)
{
other.clear();
for(int i=0;i<this.size();i++) this.add(other.get(i));
}
public void appendTo(TMList<T> other)
{
for(int i=0;i<this.size();i++) other.add(this.get(i));
}
public void appendFrom(TMList<T> other)
{
for(int i=0;i<this.size();i++) this.add(other.get(i));
}

public TMIterator<T> iterator()
{
return new TMLinkedListIterator<T>(this.start);
}

public class TMLinkedListIterator<T> implements TMIterator<T>
{
private TMLinkedListNode<T> node;
public TMLinkedListIterator(TMLinkedListNode<T> node)
{
this.node=node;
}
public boolean hasNext()
{
return this.node!=null;
}
public T next()
{
if(this.node==null) throw new InvalidIteratorException("Has No More Elements");
T data;
data=this.node.data;
this.node=this.node.next;
return data;
}
}
public void forEach(TMListItemAcceptor<T> lambda)
{
if(lambda==null) return;
TMLinkedListNode<T> t;
for(t=this.start;t!=null;t=t.next) lambda.accept(t.data);
}

}