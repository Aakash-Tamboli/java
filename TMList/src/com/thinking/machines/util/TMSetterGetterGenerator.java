package com.thinking.machines.util;
import java.lang.reflect.*;
import java.io.*;
public class TMSetterGetterGenerator
{
private static String getDefaultValue(Class c)
{
String className=c.getName();
if(className.equals("java.lang.Long") || className.equals("long")) return "0";
if(className.equals("java.lang.Integer") || className.equals("int")) return "0";
if(className.equals("java.lang.Short") || className.equals("short")) return "0";
if(className.equals("java.lang.Byte") || className.equals("byte")) return "0";
if(className.equals("java.lang.Double") || className.equals("double")) return "0";
if(className.equals("java.lang.Float") || className.equals("float")) return "0.0f";
if(className.equals("java.lang.Character") || className.equals("char")) return "' '";
if(className.equals("java.lang.Boolean") || className.equals("boolean")) return "false";
if(className.equals("java.lang.String")) return "\"\"";
return "null";
}


public static void main(String []args)
{
if(args.length!=1 &&  args.length!=2)
{
System.out.println("Usage: java classpath path_to_jar_file;. com.thinking.machines.util.TMSetterGetterGenerator class_name constructor=false_or_true");
return;
}

if(args.length==2)
{
if(args[1].equalsIgnoreCase("constructor=true")==false && args[1].equalsIgnoreCase("constructor=false")==false)
{
System.out.println("Usage: java classpath path_to_jar_file;. com.thinking.machines.util.TMSetterGetterGenerator class_name constructor=false_or_true");
return;
}
}
String className=args[0];
try
{
String setterName;
String getterName;
String tmp;
String fieldName;
String line;
Class fieldType;
Class c=Class.forName(className);
Field[] fields=c.getDeclaredFields();
Field field;
TMList<String>list=new TMArrayList<String>();

if(args.length==1 || (args.length==2 && args[1].equalsIgnoreCase("constructor=true")))
{
line="public "+c.getSimpleName()+"()";
list.add(line);
list.add("{");
for(int i=0;i<fields.length;i++)
{
field=fields[i];
line="this."+field.getName()+"="+TMSetterGetterGenerator.getDefaultValue(field.getType())+";";
list.add(line);
}
list.add("}");
}
for(int i=0;i<fields.length;i++)
{
field=fields[i];
fieldName=field.getName();
fieldType=field.getType();
if(fieldName.charAt(0)>=97 && fieldName.charAt(0)<=122)
{
// I learned about substring method where it takes start(including),end(excluding) and if we give only start point it will be consider end point to the end of the string.
tmp=fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
}
else
{
tmp=fieldName;
}
setterName="set"+tmp;
getterName="get"+tmp;
line="public void "+setterName+"("+fieldType.getName()+" "+fieldName+")";
list.add(line);
list.add("{");
line="this."+fieldName+"="+fieldName+";";
list.add(line);
list.add("}");
line="public "+fieldType.getName()+" "+getterName+"()";
list.add(line);
list.add("{");
line="return this."+fieldName+";";
list.add(line);
list.add("}");
} // for ends
File file=new File("tmp.tmp");
if(file.exists()) file.delete();
RandomAccessFile randomAccessFile;
randomAccessFile=new RandomAccessFile(file,"rw");
TMIterator<String> iterator=list.iterator();
while(iterator.hasNext())
{
line=iterator.next();
randomAccessFile.writeBytes(line+"\r\n");
}
// reason to write back slash r and back slash n is because we can go with only back slash n but there are may editors which not render new line charater in our tmp.tmp, thats why we give \r\n because its standard to do.
randomAccessFile.close();
System.out.println("setter and getter for : "+c.getName()+" is generated in name file as tmp.tmp");
}catch(ClassNotFoundException classNotFoundException)
{
System.out.println("Class Not Found");
}catch(IOException ioException)
{
System.out.println(ioException.getMessage());
}
}
}