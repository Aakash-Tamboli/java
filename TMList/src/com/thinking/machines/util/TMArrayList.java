package com.thinking.machines.util;
public class TMArrayList<T> implements TMList<T>
{
private Object[] collection;
private int capacity;
private int size;
public TMArrayList()
{
this.collection=new Object[10];
this.size=0;
this.capacity=10;
}
public void add(T data)
{
if(this.capacity==this.size)
{
Object []o;
o=new Object[this.capacity*2];
for(int i=0;i<this.size;i++)
{
o[i]=this.collection[i];
}
this.collection=o;
this.capacity=this.capacity*2;
}
this.collection[this.size]=data;
this.size++;
}

public void add(int index,T data)
{
if(index<0 || index > this.size) throw new IndexOutOfBoundsException("invalid index: "+index);
if(this.capacity==this.size)
{
Object []o;
o=new Object[this.capacity*2];
for(int i=0;i<this.size;i++)
{
o[i]=this.collection[i];
}
this.collection=o;
this.capacity=this.capacity*2;
}
for(int i=this.size;i>index;i--)
{
this.collection[i]=this.collection[i-1];
}
this.collection[index]=data;
this.size++;
}

public T removeAt(int index)
{
if(index<0 || index>=this.size) throw new IndexOutOfBoundsException("Invalid index: "+index);
for(int i=index;i<this.size;i++)
{
this.collection[i]=this.collection[i+1];
}
T data=(T)this.collection[index];
this.size--;
return data;
}

public int size()
{
return this.size;
}
public T get(int i)
{
if(i>=this.size) throw new IndexOutOfBoundsException("invalid index: "+i);
else return (T)this.collection[i];
}
public int getCapacity()
{
return this.capacity;
}
public void clear()
{
// logically it is empty technically We still have buffer
this.size=0;
}
public void insert(int index,T data)
{
this.add(index,data);
}
public void removeAll()
{
this.clear();
}
public void update(int index,T data)
{
if(index<0 || index>= this.size) throw new IndexOutOfBoundsException("invalid index: "+index);
this.collection[index]=data;
}
public void copyTo(TMList<T> other)
{
other.clear();
for(int i=0;i<this.size;i++) other.add(this.get(i));
}
public void copyFrom(TMList<T> other)
{
this.clear();
// why I call other.size() because in our interface we don't have size propery but we have size method which return the size of List.
for(int i=0;i<other.size();i++) this.add(other.get(i));
}
public void appendTo(TMList<T> other)
{
for(int i=0;i<this.size;i++) other.add(this.get(i));
}
public void appendFrom(TMList<T> other)
{
for(int i=0;i<other.size();i++) this.add(other.get(i));
}

public class TMArrayListIterator<T> implements TMIterator<T>
{
private int index;
public TMArrayListIterator(int index)
{
this.index=index;
}
public boolean hasNext()
{
return this.index!=size;
}
public T next()
{
if(index==size) throw new InvalidIteratorException("Has No More Elements");
T data=(T)get(index);
index++;
return data;
}
}

public TMIterator<T> iterator()
{
return new TMArrayListIterator<T>(0);
}

public void forEach(TMListItemAcceptor<T> lambda)
{
if(lambda==null) return;
for(int i=0;i<this.size;i++)  lambda.accept((T)this.collection[i]);
}

}