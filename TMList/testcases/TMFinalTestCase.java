import com.thinking.machines.util.*;
class Main
{
public static void main(String []args)
{
TMList<Integer> list= new TMLinkedList<Integer>();
TMList<Integer> arrList=new TMArrayList<Integer>();
for(int i=1;i<20;i++) list.add(i);
for(int i=20;i<40;i++) arrList.add(i);
list.forEach((data)->{System.out.print(data+" ");});
System.out.println();
arrList.forEach((data)->{System.out.print(data+" ");});
}
}