import com.thinking.machines.util.*;
class Main
{
public static void main(String []args)
{
TMLinkedList list=new TMLinkedList();
int data=0;
for(int i=0;i<10;i++) list.add(i+100);
System.out.println("Using For Each functionality");
list.forEach((vData)->{ System.out.println(vData);});
}
}